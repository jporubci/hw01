var total_count = 0;
var record = 0;
var coin = 1000;
var bronze = 0;
var silver = 0;
var gold = 0;
var diamond = 0;
var platinum = 0;
var random = 0;

document.getElementById('main_button').onmouseup = increment;
document.getElementById('restart_button').onmouseup = restart;

function increment() {
    random = Math.random();
    coin = coin - 100;
    if (random < 0.66) {
    } else if (random < 0.796) { // 40% chance, 13.6%
        bronze++;
        coin = coin + 50;
        document.getElementById('counter_bronze').innerHTML = bronze;
        total_count = total_count + 50;
    } else if (random < 0.898) { // 30% chance, 10.2%
        silver++;
        coin = coin + 150;
        document.getElementById('counter_silver').innerHTML = silver;
        total_count = total_count + 150;
    } else if (random < 0.9524) { // 16% chance, 5.44%
        gold++;
        coin = coin + 250;
        document.getElementById('counter_gold').innerHTML = gold;
        total_count = total_count + 250;
    } else if (random < 0.983) { // 9% chance, 3.06%
        diamond++;
        coin = coin + 1000;
        document.getElementById('counter_diamond').innerHTML = diamond;
        total_count = total_count + 1000;
    } else {                    // 5% chance, 1.7%
        platinum++;
        coin = coin + 2000;
        document.getElementById('counter_platinum').innerHTML = platinum;
        total_count = total_count + 2000;
    }
    document.getElementById('counter_coin').innerHTML = coin;

    if (coin < 100) {
        document.getElementById('main_button').disabled = true;
        document.getElementById('main_button').innerHTML = 'GAME OVER';
        document.getElementById('record_holder').innerHTML = 'You: $';
        if (total_count > record) {
            record = total_count;
            document.getElementById('record').innerHTML = record;
        }
        document.getElementById('restart_button').hidden = false;
    }
};

function restart() {
    total_count = 0;
    coin = 1000;
    bronze = 0;
    silver = 0;
    gold = 0;
    diamond = 0;
    platinum = 0;
    random = 0;
    document.getElementById('main_button').disabled = false;
    document.getElementById('main_button').innerHTML = 'Get Mineral!';
    document.getElementById('restart_button').hidden = true;
    document.getElementById('counter_bronze').innerHTML = bronze;
    document.getElementById('counter_silver').innerHTML = silver;
    document.getElementById('counter_gold').innerHTML = gold;
    document.getElementById('counter_diamond').innerHTML = diamond;
    document.getElementById('counter_platinum').innerHTML = platinum;
    document.getElementById('counter_coin').innerHTML = coin;
};
